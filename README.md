# nginx-webpage

Build the my-nginx Docker image locally
> docker build -t my-nginx .

Tag the latest local my-nginx Docker image
> docker tag my-nginx my-nginx:``<tag>``

Create a Docker container locally based on the my-nginx image with port 8080 exposed
> docker run -d --name nginx-web -p 8080:80 my-nginx

Build the my-nginx Docker image to an GitLab docker registry
> docker build -t registry.gitlab.com/bjorn.ahmark/docker-registry/my-nginx .

Tag the latest Gitlab docker registry my-nginx Docker image
> docker tag registry.gitlab.com/bjorn.ahmark/docker-registry/my-nginx registry.gitlab.com/bjorn.ahmark/docker-registry/my-nginx:``<tag>``

Push the my-nginx image changes to the GitLab docker registry
> docker push registry.gitlab.com/bjorn.ahmark/docker-registry/my-nginx
